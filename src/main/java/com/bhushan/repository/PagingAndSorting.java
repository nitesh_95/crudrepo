package com.bhushan.repository;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.bhushan.entity.CrudEntity;

public interface PagingAndSorting extends PagingAndSortingRepository<CrudEntity, Integer> {

}
