package com.bhushan.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.bhushan.entity.CrudEntity;

@Repository
public interface CrudRepoInterface extends JpaRepository<CrudEntity, Integer> {

}
