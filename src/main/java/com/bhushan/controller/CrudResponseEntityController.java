package com.bhushan.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.bhushan.entity.CrudEntity;
import com.bhushan.exception.RecordNOtFoundException;
import com.bhushan.service.CrudService;

@RestController
public class CrudResponseEntityController {

	@Autowired
	private CrudService crudService;

	@GetMapping("/getAllData")
	public ResponseEntity<List<CrudEntity>> getAllData() {
		List<CrudEntity> allData = crudService.getAllData();
		return new ResponseEntity<List<CrudEntity>>(allData, new HttpHeaders(), HttpStatus.OK);
	}

	@GetMapping("/findById/{id}")
	public ResponseEntity<CrudEntity> getById(@PathVariable int id) throws RecordNOtFoundException {
		CrudEntity entity = crudService.getById(id);
		return new ResponseEntity<CrudEntity>(entity, new HttpHeaders(), HttpStatus.OK);
	}

	@PostMapping("/createOrSave")
	public ResponseEntity<CrudEntity> createOrSave(@RequestBody CrudEntity crudEntity) {
		CrudEntity createOrUpdateEmployee = crudService.createOrUpdateEmployee(crudEntity);
		return new ResponseEntity<CrudEntity>(createOrUpdateEmployee, new HttpHeaders(), HttpStatus.OK);
	}

	@GetMapping("/deleteById/{id}")
	public String deleteById(@PathVariable int id) throws RecordNOtFoundException {
		return crudService.deleteData(id);
	}

}
