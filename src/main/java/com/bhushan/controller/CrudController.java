package com.bhushan.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.bhushan.entity.CrudEntity;
import com.bhushan.exception.RecordNOtFoundException;
import com.bhushan.service.CrudService;

@RestController
public class CrudController {

	@Autowired
	private CrudService crudService;

	@PostMapping("/addDataOrUpdateData")
	public CrudEntity createOrUpdateEmployee(@RequestBody CrudEntity crudEntity) {
		return crudService.createOrUpdateEmployee(crudEntity);
	}
	
	@GetMapping("/findAll")
	public List<CrudEntity> getAllData() {
		return crudService.getAllData();
	}
	
	@GetMapping("/findAll/{employeeid}")
	public CrudEntity findbyId(@PathVariable int employeeid) throws RecordNOtFoundException {
		return crudService.getById(employeeid);
	}
	
	@DeleteMapping("/findAll/{employeeid}")
	public String removeId(@PathVariable int employeeid) throws RecordNOtFoundException {
		return crudService.deleteData(employeeid);
	}

}