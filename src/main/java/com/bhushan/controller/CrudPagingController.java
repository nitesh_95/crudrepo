package com.bhushan.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bhushan.entity.CrudEntity;
import com.bhushan.service.PagingService;

@RestController
public class CrudPagingController {

	@Autowired
	private PagingService pagingService;

	@GetMapping("/findAllRecords")
	public ResponseEntity<List<CrudEntity>> getAllData(@RequestParam(defaultValue = "0") Integer pageno,
			@RequestParam(defaultValue = "2") Integer pagesize, @RequestParam(defaultValue = "names") String sort) {

		List<CrudEntity> allData = pagingService.getAllData(pageno, pagesize, sort);
		return new ResponseEntity<List<CrudEntity>>(allData, new HttpHeaders(), HttpStatus.OK);
	}
}
