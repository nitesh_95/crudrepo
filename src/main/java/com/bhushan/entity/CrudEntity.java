package com.bhushan.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "employeeTable")
public class CrudEntity {

	// to access the data outside we use getters and setters

	@Id
	@Column(name = "employeeid")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int employeeid;
	@Column(name = "names")
	private String names;
	@Column(name = "companyname")
	private String companyname;

	public int getEmployeeid() {
		return employeeid;
	}

	public void setEmployeeid(int employeeid) {
		this.employeeid = employeeid;
	}

	public String getName() {
		return names;
	}

	public void setName(String name) {
		this.names = name;
	}

	public String getCompanyname() {
		return companyname;
	}

	public void setCompanyname(String companyname) {
		this.companyname = companyname;
	}

}
