package com.bhushan.exception;

public class RecordNOtFoundException extends Exception{

	private static final long serialVersionUID = 1L;

	public RecordNOtFoundException(String message) {
		super(message);
	}
	
}
