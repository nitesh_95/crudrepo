package com.bhushan.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Service;

import com.bhushan.entity.CrudEntity;
import com.bhushan.repository.PagingAndSorting;

@Service
public class PagingService {

	@Autowired
	private PagingAndSorting pagingAndSorting;

	public List<CrudEntity> getAllData(Integer pageno, Integer pagesize, String sort) {
		PageRequest page = PageRequest.of(pageno, pagesize, Sort.by(sort));
		Page<CrudEntity> result = pagingAndSorting.findAll(page);

		if (result.hasContent()) {
			return result.getContent();
		} else {
			return new ArrayList<CrudEntity>();
		}

	}

}
