package com.bhushan.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bhushan.entity.CrudEntity;
import com.bhushan.exception.RecordNOtFoundException;
import com.bhushan.repository.CrudRepoInterface;

//It is also known as Stereotype annotation use to scan the class and add in the functionality of main methhod .
@Service
public class CrudService {

	@Autowired
	private CrudRepoInterface crudRepoInterface;

	public CrudEntity createOrUpdateEmployee(CrudEntity crudEntity) {
		Optional<CrudEntity> employee = crudRepoInterface.findById(crudEntity.getEmployeeid());
		if (employee.isPresent()) {
			crudEntity.setEmployeeid(crudEntity.getEmployeeid());
			crudEntity.setName(crudEntity.getName());
			crudEntity.setCompanyname(crudEntity.getCompanyname());
			CrudEntity newEntity = crudRepoInterface.save(crudEntity);
			return newEntity;

		} else {
			CrudEntity entity = crudRepoInterface.save(crudEntity);
			return entity;
		}

	}

	public List<CrudEntity> getAllData() {
		List<CrudEntity> employeeListData = crudRepoInterface.findAll();
		if (employeeListData.size() > 0) {
			return employeeListData;
		} else {
			return new ArrayList<CrudEntity>();
		}
	}

	public CrudEntity getById(int employeeId) throws RecordNOtFoundException {
		Optional<CrudEntity> employee = crudRepoInterface.findById(employeeId);
		if (employee.isPresent()) {
			return employee.get();
		} else {
			throw new RecordNOtFoundException("The Id is not Available");
		}
	}

	public String deleteData(int employeeid) throws RecordNOtFoundException {
		Optional<CrudEntity> employee = crudRepoInterface.findById(employeeid);
		if (employee.isPresent()) {
			crudRepoInterface.deleteById(employeeid);
			return "Data has been deleted";
		} else {
			throw new RecordNOtFoundException("The Id is not Available to delete");
		}
	}
}
